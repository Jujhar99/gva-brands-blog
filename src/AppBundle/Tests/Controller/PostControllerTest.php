<?php

namespace AppBundle\Tests\Controller;

use App\Form\Type\PostType;
use App\Model\PostObject;
use Symfony\Component\Form\Test\TypeTestCase;

class TestedTypeTest extends TypeTestCase
{
    public function testSubmitValidData()
    {
        $formData = array(
            'name' => 'test',
            'slug' => 'test-123',
            'category' => 'Auto maintenance',
            'author' => 'admin',
            'email' => 'test@gmail.com',
            'description' => '12',
            'description' => 'Loresm...',
        );

        $form = $this->factory->create(TestedType::class);

        $object = TestObject::fromArray($formData);

        // submit the data to the form directly
        $form->submit($formData);

        $this->assertTrue($form->isSynchronized());
        $this->assertEquals($object, $form->getData());

        $view = $form->createView();
        $children = $view->children;

        foreach (array_keys($formData) as $key) {
            $this->assertArrayHasKey($key, $children);
        }
    }
}

